package com.studies.BooksforYou.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Book {      //Book
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @NotBlank(message = "Please enter name")
    @Length(max = 255, message = "Name too long")
    private String name;
    @NotBlank(message = "Please enter description")
    @Length(max = 2048, message = "Description too long")
    private String description;
    @NotBlank(message = "Please enter a book genre")
    @Length(max = 32, message = "")
    private String genre;
    @NotBlank(message = "Please enter book author")
    @Length(max = 64, message = "Book author too long")
    private String bookAuthor;
    @NotBlank(message = "Please enter the year of writing the book")
    @Length(max = 4, message = "You entered wrong year")
    private String year;


    @ManyToOne(fetch = FetchType.EAGER)     //Одному пользователю соответствует множество сообщений     (fetch - каждый раз когда получаем сообщение мы хотим получить инфу об авторе)
    @JoinColumn(name = "user_id")       //Название поля в БД не "author", а "user_id"
    private User author;

    private String filename;        //img

    public Book() {
    }

    public Book(String name, String description, String genre, String bookAuthor, String year, User user) {
        this.author = user;
        this.name = name;
        this.description = description;
        this.genre = genre;
        this.bookAuthor = bookAuthor;
        this.year = year;
    }

    public String getAuthorName() {
        return author != null ? author.getUsername() : "<none>";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
