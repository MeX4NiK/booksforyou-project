package com.studies.BooksforYou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BooksforYouApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksforYouApplication.class, args);
	}

}
