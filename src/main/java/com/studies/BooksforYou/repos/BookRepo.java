package com.studies.BooksforYou.repos;

import com.studies.BooksforYou.domain.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookRepo extends CrudRepository<Book, Integer> {
    List<Book> findAll();
    List<Book> findByName(String name);

}
