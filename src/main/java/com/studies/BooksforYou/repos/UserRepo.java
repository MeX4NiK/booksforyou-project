package com.studies.BooksforYou.repos;

import com.studies.BooksforYou.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {       //Почему JpaRepos ?
    User findByUsername(String username);
}
