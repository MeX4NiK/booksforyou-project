package com.studies.BooksforYou.controller;

import com.studies.BooksforYou.domain.Book;
import com.studies.BooksforYou.domain.User;
import com.studies.BooksforYou.repos.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Controller
public class MainController {
    @Autowired
    private BookRepo bookRepo;

    @Value("${upload.path}")        //Получение переменной, в данном случаем он смотрит в проперти на upload.path и запихивает путь в уже нашу java переменную
    private String uploadPath;

//    @GetMapping("/")
//    public String greeting(Model model) {
//        return "greeting";
//    }

    @GetMapping("/")
    public String main(@RequestParam(required = false, defaultValue = "") String filter, Model model){
        Iterable<Book> books = bookRepo.findAll();

        if (filter != null && !filter.isEmpty()){
            books = bookRepo.findByName(filter);
        }else {
            books = bookRepo.findAll();
        }

        model.addAttribute("books", books);
        model.addAttribute("filter", filter);
        model.addAttribute("filter", "");

        return "main";
    }

    @PostMapping("/")
    public String add(
            @AuthenticationPrincipal User user,
            @Valid Book book,
            BindingResult bindingResult,    // Список аргум. и сообщений ошибок валидации   (должен идти до Model)!!!
            Model model,
            @RequestParam("file") MultipartFile file
            ) throws IOException {
        book.setAuthor(user);

        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = ControllerUtil.getErrors(bindingResult);
            model.mergeAttributes(errorsMap);
            model.addAttribute("book", book);
        } else {
            saveFile(book, file);

            model.addAttribute("book", null);    // Удаляем, иначе после добавление у нас останится открытая форма со всеми данными

            bookRepo.save(book);
        }
        Iterable<Book> books = bookRepo.findAll();
        model.addAttribute("books", books);

        return "main";
    }

    private void saveFile(@Valid Book book, @RequestParam("file") MultipartFile file) throws IOException {
        if (file != null && !file.getOriginalFilename().isEmpty()) {     // сохраняем только если задано имя файла и если файла вообще нет
            File uploadDir = new File(uploadPath);

            if (!uploadDir.exists()) {      //Проверка на существование
                uploadDir.mkdir();      //Если его не существует тогда создаем      (директория для загрузки)
            }

            //Обезопасивания от колизии (создаем уникальное имя файла)
            String uuidFile = UUID.randomUUID().toString();     //Создаем индефикатор для файла
            String resultFileName = uuidFile + "." + file.getOriginalFilename();        //Файл которое мы ложим в book

            file.transferTo(new File(uploadPath + "/" + resultFileName));      //Загружаем файл

            book.setFilename(resultFileName);    // Сохранение файла в классе
        }
    }

    @GetMapping("/user-books/{user}")
    public String userBooks(
            @AuthenticationPrincipal User currentUser,
            @PathVariable User user,
            Model model,
            @RequestParam(required = false) Book book
    ){
        Set<Book> books = user.getBooks();

        model.addAttribute("books", books);
        model.addAttribute("book", book);
        model.addAttribute("isCurrentUser", currentUser.equals(user));

        return "userBooks";
    }

    @PostMapping("/user-books/{user}")
    public String updateBook(
            @AuthenticationPrincipal User currentUser,
            @PathVariable Long user,
            @RequestParam("id") Book book,
            @RequestParam("name") String name,
            @RequestParam("description") String description,
            @RequestParam("genre") String genre,
            @RequestParam("bookAuthor") String bookAuthor,
            @RequestParam("year") String year,
            @RequestParam("file") MultipartFile file
    ) throws IOException {
        //Проверка
        if (book.getAuthor().equals(currentUser)) {
            if (!StringUtils.isEmpty(name)){
                book.setName(name);
            }

            if (!StringUtils.isEmpty(description)) {
                book.setDescription(description);
            }

            if (!StringUtils.isEmpty(genre)) {
                book.setGenre(genre);
            }

            if (!StringUtils.isEmpty(bookAuthor)) {
                book.setBookAuthor(bookAuthor);
            }

            if (!StringUtils.isEmpty(year)) {
                book.setYear(year);
            }

            saveFile(book, file);

            bookRepo.save(book);
        }

        return "redirect:/user-books/" + user;
    }

    @GetMapping("/books/{book}")
    public String Book(
            @PathVariable Book book,
            Model model
    ){
        model.addAttribute("book", book);

        return "book";
    }

}
