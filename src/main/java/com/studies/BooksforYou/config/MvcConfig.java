package com.studies.BooksforYou.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Value("${upload.path}")
    private String uploadPath;

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/img/**")          //Указываем что все картинки находятся по этому пути и перенаправляет все запросы по пути ->
                .addResourceLocations("file:/" + uploadPath + "/");    //протокол файл + путь + закрывающая черта
                        // после file: - должна быть ток одна строка
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");        //classpath - при этом обращении, ресурсы будут искатся в корне проекта
    }
}
