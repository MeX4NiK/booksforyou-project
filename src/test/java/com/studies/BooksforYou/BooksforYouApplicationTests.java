package com.studies.BooksforYou;

import com.studies.BooksforYou.controller.MainController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class BooksforYouApplicationTests {

    @Autowired
    private MainController controller;

	@Test
	public void contextLoads() throws Exception {
        assertThat(controller).isNotNull();
	}

}
